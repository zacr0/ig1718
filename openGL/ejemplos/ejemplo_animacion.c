#include <stdlib.h>
#include <iostream>
#include <GL/glut.h>

using std::cout;
using std::endl;

#define Height 400
#define Width 400

// Coordenas de la esquina superior izquierda del rectangulo
double x = 0,
       y = 0;
int direccion = 1;

void display(void) {
    glMatrixMode(GL_MODELVIEW);
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();

    glColor3f(1, 0, 0);

    // DIBUJA RECTANGULO
    glRectf(0+x, 0+y, 25+x, 25+y);

    // enviar a dibujado
    glutSwapBuffers();
}

void EscalaVentana(GLsizei w, GLsizei h) {
    // Evitar division por cero
    if (h == 0) {
        h = 1;
    }

    // Ajusta la vista a las dimensiones de la ventana
    glViewport(0, 0, w, h);

    // Reinicia el sistema de coordenadas
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // Proyeccion ortogonal centrada de la escena
    glOrtho(0, Width, 0, Height, -1.0, 1.0);

    glFlush();
}


static void reshape(int w, int h) {
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    glOrtho(0, Width, 0, Height, -1.0, 1.0);
}

void MueveCuadrado() {
    if (y < 0|| y+25 > Height || x < 0 || x+25 > Width) {
        // Cambio de direccion si se superan las dimensiones de la ventana
        cout << "CAMBIO DE DIRECCION" << endl;
        direccion *= -1;
    }
    x += 0.5 * direccion;
    y += 0.5 * direccion;

    cout << "x: " << x << endl;
    cout << "y: " << y << endl;

    glutPostRedisplay();
}

int main(int argc, char **argv) {
    glutInit(&argc, argv);
    // Double buffer orientado a animacion
    glutInitDisplayMode(GLUT_DOUBLE);
    glutInitWindowSize(Width, Height);
    glutCreateWindow("OpenGL");

    glutReshapeFunc(reshape);
    //glutReshapeFunc(EscalaVentana);
    glutIdleFunc(MueveCuadrado);
    glutDisplayFunc(display);

    // Bucle de juego
    glutMainLoop();
}