#include <GL/freeglut.h>
#include <iostream>
#include <string>

using std::cout;
using std::endl;

double window_width = 640,
       window_height = 480;

/* Dibuja un texto en pantalla */
void dibujarTextoCentrado(const char *s) {
    glColor3f(0, 0, 1);
    
    // Centrar el texto
    double tam_s = glutBitmapLength(GLUT_BITMAP_HELVETICA_18, (unsigned char *) s);
    double pos_x = (window_width - tam_s) / 2;
    double pos_y = window_height/2;
    glRasterPos2f(pos_x, pos_y);

    // Imprimir texto
    glutBitmapString(GLUT_BITMAP_HELVETICA_18, (unsigned char * ) s);
}

/* Código para modificar las variables que definen la próxima imagen */
void MyIdle(void) {
    glutPostRedisplay();
};

/* Código OpenGL que dibuja una imagen */
void MyDisplay(void) {
    // Pinta pantalla de negro
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);

    // Dibujar cruceta en centro
    glColor3f(1,0,0);
    glRectf(window_width/2,0, window_width/2+1, window_height);
    glRectf(0, window_height/2, window_width, (window_height/2)+1);

    // Muestra texto
    std::string texto("OpenGL es la mejor libreria de la historia");
    dibujarTextoCentrado(texto.c_str());
    
    /* Después de dibujar la imagen, intercambiar los buffers */
    glutSwapBuffers();
}

void reshape(int w, int h) {
    // Evitar dimension por 0
    if (h == 0) {
        h = 1;
    }

    // Ajustar viewport a dimension de ventana
    glViewport(0, 0, w, h);

    // Reiniciar sistema de coordenadas
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // Establecer espacio de trabajo al tamaño de ventana original
    glOrtho(0, window_width, 0, window_height, -1.0, 1.0);

    // Enviar a dibujado
    glutSwapBuffers();
} // reshape

int main(int argcp, char **argv) {
    /* Inicializar el estado de GLUT */
    glutInit(&argcp, argv); 
    glutInitWindowSize(window_width, window_height);
    glutInitWindowPosition(0, 0);

    /* Abrir una ventana */
    glutCreateWindow("Demo: texto en pantalla");

    /* Seleccionar el tipo de modo de display:
       Buffer doble y color RGBA */
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);

    // Funcion de redimension de ventana
    glutReshapeFunc(reshape);

    /* Registrar funciones Callback */
    glutDisplayFunc(MyDisplay);
    glutIdleFunc(MyIdle);

    /* Iniciar el procesado de eventos */
    glutMainLoop();

    return 0;
};
