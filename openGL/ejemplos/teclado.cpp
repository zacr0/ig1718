En el main con la función glutSpecialFunc:
glutSpecialFunc(TeclasMovimiento);


void TeclasMovimiento(int key, int x, int y) {
    switch (key) {
        case GLUT_KEY_RIGHT:
            MovimientoX += 0.5;
            break;
        case GLUT_KEY_LEFT:
            MovimientoX -= 0.5;
            break;
        case GLUT_KEY_UP:
            MovimientoY += 0.5;
            break;
        case GLUT_KEY_DOWN:
            MovimientoY -= 0.5;
            break;
    }
}