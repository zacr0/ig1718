#include <stdlib.h>
#include <GL/glut.h>

#define Height 400
#define Width 400


void display(void) {
    int x, y;

    glMatrixMode(GL_MODELVIEW);
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();

    glColor3f(1, 0, 0);

    // DIBUJA RECTANGULO
    glRectf(0, 0, 150., 125.);

    // Dibujar cruz de referencia
    glRectf(0, 200, 400, 201);
    glRectf(200, 0, 201, 400);

    // enviar a dibujado (diferente en caso de doble buffer)
    glFlush();
}

static void reshape(int w, int h) {
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    glOrtho(0, Width, 0, Height, -1.0, 1.0);
}

void EscalaVentana(GLsizei w, GLsizei h) {
    // Evitar division por cero
    if (h == 0) {
        h = 1;
    }

    // Ajusta la vista a las dimensiones de la ventana
    glViewport(150, 150, w, h);

    // Reinicia el sistema de coordenadas
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // Proyeccion ortogonal centrada de la escena
    glOrtho(0, Width, 0, Height, -1.0, 1.0);

    glFlush();
}

int main(int argc, char **argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE);
    glutInitWindowSize(Width, Height);
    glutCreateWindow("OpenGL");

    //glutReshapeFunc(reshape);
    glutReshapeFunc(EscalaVentana);
    // Callback de dibujado
    glutDisplayFunc(display);

    // Bucle de juego
    glutMainLoop();
}