#ifndef JUGADOR_H
#define JUGADOR_H

#include <cassert>

class Jugador {
    private:
        // Cantidada de vidas
        int _vida;
        // Puntos obtenidos
        int _puntuacion;
        // Veces consecutivas que se ha tocado el muro sin perder una vida
        int _toques;

    public:
        Jugador(const int &vida, const int &puntuacion=0, const int &toques=0) {
            set_vida(vida);
            set_puntuacion(puntuacion);
            set_toques(toques);
        }

        // Getters
        inline int get_vida() const {
            return _vida;
        }
        inline int get_puntuacion() const {
            return _puntuacion;
        }
        inline int get_toques() const {
            return _toques;
        }

        // Setters
        inline void set_vida(const int &vida) {
            assert(vida >= 0);
            _vida = vida;
        }
        inline void set_puntuacion(const int &puntuacion) {
            assert(puntuacion >= 0);
            _puntuacion = puntuacion;
        }
        inline void set_toques(const int &toques) {
            assert(toques >= 0);
            _toques = toques;
        }
};
#endif