#ifndef PELOTA_H
#define PELOTA_H

#include <cassert>

class Pelota {
    private:
        // Tamaño: lado del rectangulo
        int  _tamano;
        // Coordenadas de la esquina inferior izquierda
        float _x;
        float _y;
        float _x_inicial;
        float _y_inicial;
        // Direccion de movimiento
        float _dir_x;
        float _dir_y;
        // Velocidad de desplazamiento
        float _velocidad;
        float _velocidad_inicial;
    
    public:
        Pelota(const float &x, const float &y, const int &tam, const float &dir_x, const float &dir_y, const float &v) {
            set_tamano(tam);
            set_x(x);
            set_y(y);
            set_x_inicial(x);
            set_y_inicial(y);
            set_dir_x(dir_x);
            set_dir_y(dir_y);
            set_velocidad(v);
            set_velocidad_inicial(v);
        }

        // Getters
        inline int get_tamano() const {
            return _tamano;
        }
        inline float get_x() const {
            return _x;
        }
        inline float get_y() const {
            return _y;
        }
        inline float get_x_inicial() const {
            return _x_inicial;
        }
        inline float get_y_inicial() const {
            return _y_inicial;
        }
        inline float get_dir_x() const {
            return _dir_x;
        }
        inline float get_dir_y() const {
            return _dir_y;
        }
        inline float get_velocidad() const {
            return _velocidad;
        }
        inline float get_velocidad_inicial() const {
            return _velocidad_inicial;
        }

        // Setters
        inline void set_tamano(const int &tam) {
            assert(tam > 0);
            _tamano = tam;
        }
        inline void set_x(const float &x) {
            _x = x;
        }
        inline void set_y(const float &y) {
            _y = y;
        }
        inline void set_x_inicial(const float &x) {
            _x_inicial = x;
        }
        inline void set_y_inicial(const float &y) {
            _y_inicial = y;
        }
        inline void set_dir_x(const float &dir_x) {
            _dir_x = dir_x;
        }
        inline void set_dir_y(const float &dir_y) {
            _dir_y = dir_y;
        }
        inline void set_velocidad(const float &v) {
            assert(v >= 0);
            _velocidad = v;
        }
        inline void set_velocidad_inicial(const float &v) {
            assert(v >= 0);
            _velocidad_inicial = v;
        }
};
#endif