#ifndef VENTANA_H
#define VENTANA_H

#include <cassert>

class Ventana {
    private:
        // ID de la ventana openGL
        int _id;
        // Dimensiones
        int _ancho;
        int _alto;
        // Tasa de actualizacion de la escena
        float _tasa_refresco;
    
    public:
        Ventana(const int &id, const int &ancho, const int &alto, const float &tasa_refresco) {
            set_id(id),
            set_ancho(ancho);
            set_alto(alto);
            set_tasa_refresco(tasa_refresco);
        }

        // Getters
        inline int get_id() const {
            return _id;
        }
        inline int get_ancho() const {
            return _ancho;
        }
        inline int get_alto() const {
            return _alto;
        }
        inline float get_tasa_refresco() const {
            return _tasa_refresco;
        }

        // Setters
        inline void set_id(const int &id) {
            _id = id;
        }
        inline void set_ancho(const int &ancho) {
            assert(ancho > 0);
            _ancho = ancho;
        }
        inline void set_alto(const int &alto) {
            assert(alto > 0);
            _alto = alto;
        }
        inline void set_tasa_refresco(const float &tasa_refresco) {
            assert(tasa_refresco > 0);
            _tasa_refresco = tasa_refresco;
        }
};
#endif