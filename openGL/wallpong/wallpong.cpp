#include <string>
#include <iostream>
#include <math.h>
#include "GL/freeglut.h"
#include "Ventana.hpp"
#include "Jugador.hpp"
#include "Raqueta.hpp"
#include "Pelota.hpp"

using std::string;
using std::to_string;
using std::cout;
using std::endl;

/*
* VARIABLES GLOBALES
*/
// VENTANA DE JUEGO
Ventana ventana = Ventana(-1, 600, 350, 1000 / 60);
// JUGADOR
Jugador jugador = Jugador(5);
// RAQUETA
Raqueta raqueta = Raqueta(10, (ventana.get_alto() - 80) / 2, 10, 80, 5);
// MURO
Raqueta muro = Raqueta(ventana.get_ancho() - 20, 0, 20, ventana.get_alto(), 0);
// PELOTA
Pelota pelota = Pelota((ventana.get_ancho() - 14) / 2, (ventana.get_alto() - 14) / 2, 14, -1, 0, 4);

/*
* FUNCIONES
*/
// Muestra una cadena de texto en las coordenadas especificadas, con un tamaño de fuente mediano
void mostrarTextoMedio(const float &x, const float &y, const string &text) {
    glRasterPos2f(x, y);
    glutBitmapString(GLUT_BITMAP_9_BY_15, (const unsigned char*) text.c_str());
}

// Muestra una cadena de texto en las coordenadas especificadas, con un tamaño de fuente grande
void mostrarTextoGrande(const float &x, const float &y, const string &text) {
    glRasterPos2f(x, y);
    glutBitmapString(GLUT_BITMAP_TIMES_ROMAN_24, (const unsigned char*) text.c_str());
}

// Logica de dibujado de la escena
void draw() {
    string texto;
    int longitud_texto;

    // Limpiar pantalla
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    if (jugador.get_vida() > 0) {
        // Dibujar raqueta y muro en blanco
        glColor3f(1, 1, 1);
        glRectf(raqueta.get_x(), raqueta.get_y(), raqueta.get_x() + raqueta.get_ancho(), raqueta.get_y() + raqueta.get_alto());
        glRectf(muro.get_x(), muro.get_y(), muro.get_x() + muro.get_ancho(), muro.get_alto());
        

        // Dibujar pelota con un color mas intenso a mayor sea su velocidad
        glColor3f(1, 1 * (4 / pelota.get_velocidad()), 1 * (4 / pelota.get_velocidad()));
        glRectf(pelota.get_x(), pelota.get_y(), pelota.get_x() + pelota.get_tamano(),pelota.get_y() + pelota.get_tamano());


        // Dibujar vida y puntuacion en la esquina superior izquierda
        // Color de vida en funcion de las vidas restantes, menos vidas -> mas rojo
        glColor3f(1, 1 * (jugador.get_vida() / 3), 1 * (jugador.get_vida() / 3));
        texto = "Vidas: " + to_string(jugador.get_vida());
        mostrarTextoMedio(25, ventana.get_alto() - 15, texto);
        // Obtener longitud en pixeles de esta cadena para determinar donde colocar la siguiente 
        longitud_texto = glutBitmapLength(GLUT_BITMAP_9_BY_15, (unsigned char *) texto.c_str());

        // Dibujar puntuacion en blanco
        glColor3f(1, 1, 1);
        texto = "Puntuacion: " + to_string(jugador.get_puntuacion());
        mostrarTextoMedio(55 + longitud_texto, ventana.get_alto() - 15, texto); 
    } else {
        // Pantalla de fin de juego
        glColor3f(1, 0, 0);
        texto = "YOU DIED";
        longitud_texto = glutBitmapLength(GLUT_BITMAP_TIMES_ROMAN_24 , (unsigned char *) texto.c_str());
        mostrarTextoGrande((ventana.get_ancho() - longitud_texto) / 2, (ventana.get_alto() / 2) + 50, texto);

        // Mostrar estadisticas de juego
        glColor3f(1, 1, 1);
        texto = "- Puntuacion final -";
        longitud_texto = glutBitmapLength(GLUT_BITMAP_9_BY_15 , (unsigned char *) texto.c_str());
        mostrarTextoMedio((ventana.get_ancho() - longitud_texto) / 2, (ventana.get_alto() / 2), texto);
        texto = to_string(jugador.get_puntuacion());
        longitud_texto = glutBitmapLength(GLUT_BITMAP_TIMES_ROMAN_24 , (unsigned char *) texto.c_str());
        mostrarTextoGrande((ventana.get_ancho() - longitud_texto) / 2, (ventana.get_alto() / 2) - 30, texto);

        // Mostrar informacion de teclas de accion
        texto = "INTRO: Nueva partida      ESCAPE: Salir del juego";
        longitud_texto = glutBitmapLength(GLUT_BITMAP_9_BY_15 , (unsigned char *) texto.c_str());
        mostrarTextoMedio((ventana.get_ancho() - longitud_texto) / 2, (ventana.get_alto() / 2) - 100, texto);        
    }

    // test: cruceta centrada
    /*glRectf(0, (ventana.get_alto() / 2) - 1, ventana.get_ancho(), (ventana.get_alto() / 2) + 1);
    glRectf((ventana.get_ancho() / 2) - 1, 0, (ventana.get_ancho() / 2) + 1, ventana.get_alto());*/

    // Dibujar frame
    glutSwapBuffers();
}

// Logica de las teclas captables mediante GLUT, controlan el movimieno del jugador
void leerTeclasMovimiento(int tecla, int x, int y) {
    float nueva_posicion;

    switch (tecla) {
        case GLUT_KEY_UP:
            nueva_posicion = raqueta.get_y() + raqueta.get_velocidad();
            // Actualizar posicion si no supera el limite superior
            if ((nueva_posicion + raqueta.get_alto()) <= ventana.get_alto()) {
                raqueta.set_y(raqueta.get_y() + raqueta.get_velocidad());
            }
            break;
        case GLUT_KEY_DOWN:
            nueva_posicion = raqueta.get_y() - raqueta.get_velocidad();
            // Actualizar posicion si no supera el limite inferior
            if (nueva_posicion >= 0) {
                raqueta.set_y( raqueta.get_y() - raqueta.get_velocidad());
            }
            break;
    }
}

// Logica de las teclas de juego captables con codigo ASCII, controlan la 
// salida y reinicio del juego
void leerTeclasJuego(unsigned char tecla, int x, int y) {
    switch (tecla) {
        case 13:
            // Tecla INTRO, reiniciar partida (en pantalla de fin de juego)
            if (jugador.get_vida() <= 0) {
                jugador.set_vida(5);
                jugador.set_puntuacion(0);
                jugador.set_toques(0);
                raqueta.set_x(raqueta.get_x_inicial());
                raqueta.set_y(raqueta.get_y_inicial());
                pelota.set_velocidad( pelota.get_velocidad_inicial() );
            }
            break;
        case 27:
            // Tecla ESCAPE, cerrar aplicacion
            glutDestroyWindow(ventana.get_id());
            exit(0);
            break;
    }
}

// Normaliza un vector, hacieno que su longitud sea igual a 1
void vec2_norm(float &x, float &y) {
    float longitud = sqrt( pow(x, 2) + pow(y,2) );

    if (longitud != 0.0f) {
        longitud = 1.0f / longitud;
        x *= longitud;
        y *= longitud;
    }
}

// Actualiza la velocidad de desplazamiento de la pelota, si cumple los criterios
void actualizarVelocidad() {
    if (jugador.get_puntuacion() > 0 && 
        jugador.get_puntuacion() % 5 == 0) {
        pelota.set_velocidad( pelota.get_velocidad() * 1.15f );
    }
}

// Actualiza la puntuacion del jugador, si cumple los criterios
void actualizarPuntuacion() {
    if (jugador.get_toques() > 0 &&
        jugador.get_toques() % 3 == 0) {
        jugador.set_puntuacion( jugador.get_puntuacion()+1 );
        actualizarVelocidad();
    }
}

// Fisicas de desplazamiento y colision de la pelota
void moverPelota() {
    // Desplazar pelota segun velocidad
    pelota.set_x( pelota.get_x() + pelota.get_dir_x() * pelota.get_velocidad());
    pelota.set_y( pelota.get_y() + pelota.get_dir_y() * pelota.get_velocidad());
   
    // Colision con raqueta
    if (pelota.get_x() < (raqueta.get_x() + raqueta.get_ancho()) &&
        pelota.get_x() > raqueta.get_x() &&
        ((pelota.get_y() < (raqueta.get_y() + raqueta.get_alto()) &&
        pelota.get_y() > raqueta.get_y()) || 
        ((pelota.get_y() + pelota.get_tamano()) < (raqueta.get_y() + raqueta.get_alto()) &&
         (pelota.get_y() + pelota.get_tamano()) > raqueta.get_y()))) {
        /*
         * Establecer direccion del movimiento segun la parte donde haya impactado
         * Parte superior -> t = 0.5
         * Centro -> t = 0, 
         * Parte inferior -> t = -0.5
         */
        float t = ((pelota.get_y() - raqueta.get_y()) / raqueta.get_alto()) - 0.5f;
        pelota.set_dir_x( fabs(pelota.get_dir_x()) ); // direccion positiva (derecha)
        pelota.set_dir_y(t);
    }
   
    // Colision con muro
    if ((pelota.get_x() + pelota.get_tamano()) > muro.get_x() &&
        (pelota.get_x() + pelota.get_tamano()) < (muro.get_x() + muro.get_ancho()) &&
        (pelota.get_y() + pelota.get_tamano()) < (muro.get_y() + muro.get_alto()) &&
        pelota.get_y() > muro.get_y()) {
        /*
         * Establecer direccion del movimiento segun la parte donde haya impactado
         * Parte superior -> t = 0.5
         * Centro -> t = 0, 
         * Parte inferior -> t = -0.5
         */
        float t = ((pelota.get_y() - muro.get_y()) / muro.get_alto()) - 0.5f;
        pelota.set_dir_x( -fabs(pelota.get_dir_x()) ); // direccion negativa (izquierda)
        pelota.set_dir_y(t);

        // Actualizar contador de pelotas devueltas al muro
        jugador.set_toques( jugador.get_toques()+1 );
        // Actualizar puntuacion y velocidad si el numero de toques es suficiente
        actualizarPuntuacion();
    }

    // Colision con extremo izquierdo
    if (pelota.get_x() < 0) {
        // El jugador pierde una vida y su contador de toques se reinicia
        jugador.set_vida( jugador.get_vida()-1 );
        jugador.set_toques(0);

        // Reiniciar posicion y direccion de pelota
        pelota.set_x(pelota.get_x_inicial());
        pelota.set_y(pelota.get_y_inicial());
        pelota.set_dir_x( fabs(pelota.get_dir_x()) ); // direccion positiva (derecha)
        pelota.set_dir_y(0);

        // Reiniciar posicion de la raqueta
        raqueta.set_x( raqueta.get_x_inicial() );
        raqueta.set_y( raqueta.get_y_inicial() );
    }

    // Colision con extremo superior
    if ((pelota.get_y() + pelota.get_tamano()) > ventana.get_alto()) {
        pelota.set_dir_y( -fabs(pelota.get_dir_y()) ); // direccion negativa (abajo)
    }

    // Colision con extremo inferior
    if (pelota.get_y() < 0) {
        pelota.set_dir_y( fabs(pelota.get_dir_y()) ); // direccion positiva (arriba)
    }

    // Normalizar longitud del vector de movimiento
    float dir_x = pelota.get_dir_x(),
          dir_y = pelota.get_dir_y();
    vec2_norm(dir_x, dir_y);
}

// Actualizacion de ventana
void update(int valor) {
    // Llamada a update en funcion de la tasa de refresco (ms)
    glutTimerFunc(ventana.get_tasa_refresco(), update, 0);
    
    if (jugador.get_vida() > 0) {
        // Actualizar posicion de la pelota
        moverPelota();
    }

    // Dibujar
    glutPostRedisplay();
}

// Establece el escenario como 2D
void enable2D(const int &ancho, const int &alto) {
    glViewport(0, 0, ancho, alto);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0f, ancho, 0.0f, alto, 0.0f, 1.0f);
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity();
}


/*
* PROGRAMA PRINCIPAL
*/
int main(int argc, char **argv) {
    // Inicializar openGL
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(ventana.get_ancho(), ventana.get_alto());
    ventana.set_id( glutCreateWindow("WallPong") );

    // Registrar funciones de dibujado
    glutDisplayFunc(draw);
    glutTimerFunc(ventana.get_tasa_refresco(), update, 0);

    // Registrar funciones de teclado
    glutSpecialFunc(leerTeclasMovimiento);
    glutKeyboardFunc(leerTeclasJuego);

    // Configurar escenario como 2D 
    enable2D(ventana.get_ancho(), ventana.get_alto());

    // Bucle de juego
    glutMainLoop();

    return EXIT_SUCCESS;
}
