#ifndef RAQUETA_H
#define RAQUETA_H

#include <cassert>

class Raqueta {
    private:
        // Dimensiones
        int _ancho;
        int _alto;
        // Coordenadas de la esquina inferior izquierda
        float _x_inicial;
        float _y_inicial;
        float _x;
        float _y;
        // Velocidad de desplazamiento
        float _velocidad;

    public:
        Raqueta(const float &x, const float &y, const int &ancho, const int &alto, const float &velocidad) {
            set_x(x);
            set_y(y);
            set_x_inicial(x);
            set_y_inicial(y);
            set_ancho(ancho);
            set_alto(alto);
            set_velocidad(velocidad);
        }

        // Getters
        inline int get_ancho() const {
            return _ancho;
        }
        inline int get_alto() const {
            return _alto;
        }
        inline float get_x() const {
            return _x;
        }
        inline float get_y() const {
            return _y;
        }
        inline float get_x_inicial() const {
            return _x_inicial;
        }
        inline float get_y_inicial() const {
            return _y_inicial;
        }
        inline float get_velocidad() const {
            return _velocidad;
        }

        // Setters
        inline void set_ancho(const int &ancho) {
            assert(ancho > 0);
            _ancho = ancho;
        }
        inline void set_alto(const int &alto) {
            assert(alto > 0);
            _alto = alto;
        }
        inline void set_x(const float &x) {
            _x = x;
        }
        inline void set_y(const float &y) {
            _y = y;
        }
        inline void set_x_inicial(const float &x) {
            _x_inicial = x;
        }
        inline void set_y_inicial(const float &y) {
            _y_inicial = y;
        }
        inline void set_velocidad(const float &velocidad) {
            _velocidad = velocidad;
        }
};

#endif