/*
* @file     main_animacion.cpp
* @brief    Aplicacion en openGL que muestra un cuadrado desplazandose por la 
* pantalla hasta volver al punto de inicio.
* @author   Pablo Medina Suarez
*/
#include <GL/glut.h>
#include <iostream>
#include <stdlib.h>

using std::cout;
using std::endl;

// VARIABLES GLOBALES
// Tamaño de ventana del programa
double Alto = 400,
       Ancho = 400;
// Tamaño del rectangulo
double tam_x = 100,
       tam_y = 100;
// Velocidad de desplazamiento
double velocidad = 3;
// Coordenadas de la esquina inferior izquierda del rectangulo
double x = 0,
       y = 0;
/*
*   Vectores de direccion horizontal y vertical
*   +1: derecha / arriba
*   -1: izquierda / abajo
*/
int dir_x = 1,
    dir_y = 1;


// DECLARACION DE FUNCIONES
/*
* Logica de dibujado de la escena
*/
void display(void);
/*
* Logica de redimension de ventana de la aplicacion
*/
void reshape(int w, int h);
/*
* Logica de la animacion estatica encargada de desplazar el cuadrado por
* la pantalla.
*/
void moverCuadrado();
/*
* Comprueba si el cuadrado ha completado el recorrido, es decir, ha vuelto al
* punto de inicio viniendo en direccion opuesta.
*/
bool haAlcanzadoFin();
/*
* Comprueba valores limite de coordenadas y establecer cambio de direccion
*/
void actualizarDireccion();

// PROGRAMA PRINCIPAL
int main(int argc, char **argv) {
    glutInit(&argc, argv);

    // Inicializacion de ventana
    glutInitDisplayMode(GLUT_DOUBLE);
    glutInitWindowSize(Ancho, Alto);
    glutCreateWindow("OpenGL");

    // Funcion de redimension de ventana
    glutReshapeFunc(reshape);

    // Animacion estatica
    glutIdleFunc(moverCuadrado);

    // Dibujar escena
    glutDisplayFunc(display);

    // Bucle de eventos
    glutMainLoop();

    return EXIT_SUCCESS;
} // main


// IMPLEMENTACION DE FUNCIONES
void display(void) {
    glMatrixMode(GL_MODELVIEW);
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();

    // Color del rectangulo
    if (haAlcanzadoFin()) {
        // Verde al finalizar
        glColor3f(0, 1, 0);
        cout << " -- Fin --" << endl;
    } else {
        // Amarillo durante recorrido
        glColor3f(1, 1, 0);
    }
    // Dibujar rectangulo
    glRectf(0+x, 0+y, tam_x+x, tam_y+y);


    // Dibujar camino recorrido en filas anteriores
    if (dir_y == 1) {
        // Camino de ida (rojo)
        glColor3f(1, 0, 0);
        // Pintar camino recorrido en fila actual
        if (dir_x == 1) {
            // De izquierda a derecha
            glRectf(0, y, x, tam_y+y);
        } else {
            // De derecha a izquierda
            glRectf(Ancho, y, x+tam_x, tam_y+y);
        }
        // Pintar filas anteriores
        glRectf(0, 0, Ancho, y);
    } else {
        // Camino de vuelta
        // Pintar camino pendiente de caminar en fila actual (rojo)
        glColor3f(1, 0, 0);
        if (dir_x == 1) {
            // De izquierda a derecha
            glRectf(x+tam_x, y, Ancho, tam_y+y);
        } else {
            // De derecha a izquierda
            glRectf(0, y, x, tam_y+y);
        }
        // Pintar filas anteriores
        glRectf(0, 0, Ancho, y);

        // Camino recorrido (azul)
        glColor3f(0, 0, 1);
        // Pintar fila actual
        if (dir_x == 1) {
            // De izquierda a derecha
            glRectf(0, y, x, tam_y+y);
        } else {
            // De derecha a izquierda
            glRectf(Ancho, y, x+tam_x, tam_y+y);
        }
        // Pintar filas anteriores
        glRectf(0, Alto, Ancho, tam_y+y);
    }

    // Dibujar escena
    glutSwapBuffers();
} // display

void reshape(int w, int h) {
    // Evitar dimension por 0
    if (h == 0) {
        h = 1;
    }

    // Ajustar viewport a dimension de ventana
    glViewport(0, 0, w, h);

    // Reiniciar sistema de coordenadas
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // Establecer espacio de trabajo al tamaño de ventana original
    glOrtho(0, Ancho, 0, Alto, -1.0, 1.0);

    // Enviar a dibujado
    glutSwapBuffers();
} // reshape

void moverCuadrado() {
    if (!haAlcanzadoFin()) {
        // Desplazar horizontalmente
        x += velocidad * dir_x;
        // Desplazar verticalmente, si se alcanza un extremo derecho
        if ( (x+tam_x) >= Ancho ) {
            y += tam_y * dir_y;
        }
        // Desplazar verticalmente, si se alcanza un extremo izquierdo que no sea 
        // una esquina
        if ( (x <= 0) && (y != 0) && ((y+tam_y) != Alto) ) {
            y += tam_y * dir_y;
        }
        cout << "x: " << x << endl;
        cout << "y: " << y << endl;

        if (!haAlcanzadoFin()) {
            // Comprobar limites y actualizar direccion de movimiento
            actualizarDireccion();
        }

        // Dibujar
        glutPostRedisplay();
    }
} // moverCuadrado

bool haAlcanzadoFin() {
    if (x <= 0 && y <= 0 && dir_y == -1) {
        return true;
    }
    return false;
} //haAlcanzadoFin

void actualizarDireccion() {
    // Esta en un extremo horizontal
    if ( (x <= 0 && dir_x == -1) || ((x+tam_x) >= Ancho) ) {
        // Invertir direccion horizontal
        dir_x *= -1;
        cout << "CAMBIO DE DIRECCION EN X" << endl;

        // Esta en un extremo vertical
        if ((y <= 0 && dir_x == 1) || (((y+tam_y) >= Alto) && dir_x == 1) ) {
            // Invertir direccion vertical
            dir_y *= -1;
            cout << "CAMBIO DE DIRECCION EN Y" << endl;
        }
    }
} // actualizarDireccion
