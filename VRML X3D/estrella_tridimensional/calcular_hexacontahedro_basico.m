% Partir del dodecaedro inicial
puntos = [1.21412     0           1.58931;
          0.375185    1.1547      1.58931;
          -0.982247   0.713644    1.58931;
          -0.982247   -0.713644   1.58931;
          0.375185    -1.1547     1.58931;      
          1.96449     0           0.375185;
          0.607062    1.86835     0.375185;
          -1.58931    1.1547      0.375185;
          -1.58931    -1.1547     0.375185;
          0.607062    -1.86835    0.375185;
          1.58931     1.1547      -0.375185;
          -0.607062   1.86835     -0.375185;
          -1.96449    0           -0.375185;
          -0.607062   -1.86835    -0.375185;
          1.58931     -1.1547     -0.375185;
          0.982247    0.713644    -1.58931;
          -0.375185   1.1547      -1.58931;
          -1.21412    0           -1.58931;
          -0.375185   -1.1547     -1.58931;
          0.982247    -0.713644   -1.58931];

caras = [0, 1, 2, 3, 4; 
         0, 5, 10, 6, 1; 
         1, 6, 11, 7, 2; 
         2, 7, 12, 8, 3;
         3, 8, 13, 9, 4; 
         4, 9, 14, 5, 0; 
         15, 10, 5, 14, 19;
         16, 11, 6, 10, 15; 
         17, 12, 7, 11, 16; 
         18, 13, 8, 12, 17; 
         19, 14, 9, 13, 18; 
         19, 18, 17, 16, 15];

% Calcular el punto medio de cada cara inicial
puntos_medios_caras = zeros(size(caras,1), 3);
nuevas_caras = [];
colores = [];
for i=1:length(caras)
    fprintf('Cara %d:\n', i);
    puntos_cara = caras(i,:);
    
    % Calcular el punto medio de la cara
    punto_medio = [mean( puntos(puntos_cara+1,1) ), ...
                   mean( puntos(puntos_cara+1,2) ), ...
                   mean( puntos(puntos_cara+1,3) )];
    
    % Reducir el punto central a la mitad (hundirlo)
    puntos_medios_caras(i,:) = punto_medio * .5;
    disp('Punto medio:');
    disp(punto_medio);
    
    % Creamos 5 caras a partir de la cara inicial actual y el punto medio
    % (1 cara pasa a tener 5 caras triangulares)
    for j=1:length(puntos_cara)
        if j+1 > 5
            % Cara del ultimo punto con el primero
            nueva_cara = [puntos_cara(j), puntos_cara(1), length(puntos)+i-1, -1];
        else
            % Cara del punto actual con el siguiente
            nueva_cara = [puntos_cara(j), puntos_cara(j+1), length(puntos)+i-1, -1];
        end
        nuevas_caras = [nuevas_caras; nueva_cara];
        
        % Generar color aleatorio para la nueva cara
        colores = [colores; rand(1, 3)];
    end
end

% Valores finales
% Puntos
puntos = [puntos; puntos_medios_caras];
disp('Puntos:');
disp(puntos);

% Caras
disp('Caras:');
disp(nuevas_caras);

% Colores
disp('Colores:');
disp(colores);