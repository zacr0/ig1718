% Genera un hexacontaedro rombico a partir de los puntos y caras de un
% dodecaedro.
% Asigna un color aleatorio a cada cara (rombo) generado, para facilitar su
% identificacion.

% Partir del dodecaedro inicial
puntos = [1.21412     0           1.58931;
          0.375185    1.1547      1.58931;
          -0.982247   0.713644    1.58931;
          -0.982247   -0.713644   1.58931;
          0.375185    -1.1547     1.58931;      
          1.96449     0           0.375185;
          0.607062    1.86835     0.375185;
          -1.58931    1.1547      0.375185;
          -1.58931    -1.1547     0.375185;
          0.607062    -1.86835    0.375185;
          1.58931     1.1547      -0.375185;
          -0.607062   1.86835     -0.375185;
          -1.96449    0           -0.375185;
          -0.607062   -1.86835    -0.375185;
          1.58931     -1.1547     -0.375185;
          0.982247    0.713644    -1.58931;
          -0.375185   1.1547      -1.58931;
          -1.21412    0           -1.58931;
          -0.375185   -1.1547     -1.58931;
          0.982247    -0.713644   -1.58931];

caras = [0, 1, 2, 3, 4; 
         0, 5, 10, 6, 1; 
         1, 6, 11, 7, 2; 
         2, 7, 12, 8, 3;
         3, 8, 13, 9, 4; 
         4, 9, 14, 5, 0; 
         15, 10, 5, 14, 19;
         16, 11, 6, 10, 15; 
         17, 12, 7, 11, 16; 
         18, 13, 8, 12, 17; 
         19, 14, 9, 13, 18; 
         19, 18, 17, 16, 15];

% Calcular puntos medios de cada cara y entre vertices de cara
puntos_medios_caras = zeros(size(caras,1), 3);
puntos_medios_vertices = [];
nuevas_caras = [];
colores = [];
for i=1:length(caras)
    fprintf('Cara %d:\n', i);
    puntos_cara = caras(i,:);
    
    % Calcular el punto medio de la cara
    punto_medio_cara = [mean( puntos(puntos_cara+1,1) ), ...
                   mean( puntos(puntos_cara+1,2) ), ...
                   mean( puntos(puntos_cara+1,3) )];
    
    % Reducir el punto central a la mitad (hundirlo)
    puntos_medios_caras(i,:) = punto_medio_cara * .5;
    disp('Punto medio (cara):');
    disp(punto_medio_cara);
    
    % Calcular un punto medio entre cada 2 vertices de la cara
    for j=1:length(puntos_cara)
        if j+1 > 5
            % Vertice medio entre el ultimo y el primero
            punto_medio_vertice = mean([ puntos(puntos_cara(1)+1, :); ... 
                                         puntos(puntos_cara(j)+1,:) ]) * .75;
        else
            % Vertice medio entre el actual y el siguiente
            punto_medio_vertice = mean([ puntos(puntos_cara(j)+1, :);
                                         puntos(puntos_cara(j+1)+1, :) ]) * .75;
        end

        % Agregar el nuevo punto medio calculado a la lista
        puntos_medios_vertices = [puntos_medios_vertices; punto_medio_vertice];
    end
    
    % Convertir una cara del dodecaedro original en 5 caras rombicas.
    % Los extremos verticales son un vertice del pentagono y el punto medio 
    % de la cara.
    % Los extremos horizontales son los puntos medios entre vertices.
    for j=1:length(puntos_cara)
        if j == 1
            % Cara del primer vertice con el ultimo
            nueva_cara = [puntos_cara(j), 31+5*(i-1)+j, length(puntos)+i-1, 31+5*(i-1)+5, -1];
        else
            % Cara del vertice actual con el siguiente
            nueva_cara = [puntos_cara(j), 31+5*(i-1)+j, length(puntos)+i-1, 31+5*(i-1)+j-1, -1];
        end
        
        % Agregar la cara obtenida a la lista de caras
        nuevas_caras = [nuevas_caras; nueva_cara];
        
        % Generar color aleatorio para la nueva cara
        colores = [colores; rand(1, 3)];
    end
end

% Valores finales
% Puntos
puntos = [puntos; puntos_medios_caras; puntos_medios_vertices];
disp('Puntos:');
disp(puntos);

% Caras
disp('Caras:');
disp(nuevas_caras);

% Colores
disp('Colores:');
disp(colores);